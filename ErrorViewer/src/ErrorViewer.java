import javax.net.ssl.HttpsURLConnection;
import java.io.*;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import org.json.*;

/**
 * Created by danielbraithwt on 9/26/15.
 */
public class ErrorViewer {

    public static void main(String[] args) {

        // First signin to raygun and get the auth information to access
        // the api
        System.out.println("Signing In...");
        RaygunConnection connection = new RaygunConnection("danielbraithwt@gmail.com", "raygunpassword");

        // Now we can access the api to get all the errors
        System.out.println("Retreving Error Data...");
        String errors = connection.getUrl("https://webapi.raygun.io/applications/886888661/errors/all");

        // Convert the string to a JSON object
        JSONObject allErrors = new JSONObject(errors);
        JSONArray active = allErrors.getJSONObject("activeErrors").getJSONArray("records");

        // Loop through the active errors and print out the first
        // 5 or less
        System.out.println();
        for (int i = 0; i < active.length() && i < 5; i++) {
            JSONObject current = active.getJSONObject(i);

            int id = current.getInt("id");
            String message = current.getString("message");
            int count = current.getInt("count");

            System.out.println("Error: " + (i + 1));
            System.out.println(" ID: " + id);
            System.out.println(" Message: " + message);
            System.out.println(" Count: " + count);
        }

        // Ask the user for which error they want to see the raw JSON for
        Scanner in = new Scanner(System.in);
        int option = -1;
        while (option == -1) {
            System.out.print("Which error would you like to view the raw JSON for?: ");
            int input = in.nextInt();

            if (input >= 1 || input <= active.length()) {
                option = input-1;
            } else {
                System.out.println("That option is not valid");
            }
        }

        System.out.println("\nGetting JSON");

        // Use our connection to access the url where the JSON is
        String selectedJSON = connection.getUrl("https://webapi.raygun.io/errors/" + active.getJSONObject(option).getInt("id") + "/occurrences/latest");

        // Turn it into a JSON object and get the details
        JSONObject selectedJSONObj = new JSONObject(selectedJSON);
        System.out.println(selectedJSONObj.getJSONObject("details"));
    }

}
