import javax.net.ssl.HttpsURLConnection;
import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Map;

/**
 * Created by danielbraithwt on 9/29/15.
 */
public class RaygunConnection {
    private final List<String> cookie;

    /**
     * Connects to raygun and signs in. Saves the cookies
     * so connections can be made later
     * @param email
     * @param password
     */
    public RaygunConnection(String email, String password) {
        String urlParams = String.format("EmailAddress=%s&Password=%s&RemeberMe=false", email, password);
        byte[] postData = urlParams.getBytes(StandardCharsets.UTF_8);


        try {
            URL raygunUrl = new URL("https://app.raygun.io/signin");
            HttpsURLConnection con = (HttpsURLConnection) raygunUrl.openConnection();

            con.setDoOutput(true);
            con.setInstanceFollowRedirects(false);
            con.setRequestMethod("POST");
            con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            con.setRequestProperty("charset", "utf-8");
            con.setRequestProperty("Content-Length", Integer.toString(postData.length));
            con.setUseCaches(false);

            // Write our login information
            DataOutputStream o = new DataOutputStream(con.getOutputStream());
            o.write(postData);
            o.flush();
            o.close();

            // Get the responce headers, specificly the cookie
            Map<String, List<String>> headers = con.getHeaderFields();
            cookie = headers.get("Set-Cookie");
        } catch (IOException e) {
            throw new RuntimeException("An error occored while logging in to raygun");
        }
    }

    /**
     * Use our connection to raygun to access the given url
     *
     * @param u The url to visit
     * @return The data at the url
     */
    public String getUrl(String u) {
        // Ensure the url is a raygun url
        if (!u.contains(".raygun.io")) {
            throw new RuntimeException("Must be a raygun url");
        }

        try {
            HttpsURLConnection con = (HttpsURLConnection) new URL(u).openConnection();
            con.setRequestMethod("GET");
            con.setRequestProperty("Cookie", cookie.get(0) + cookie.get(1) + cookie.get(2));
            con.setRequestProperty("X-ApiKey", "f8MHm8bqiYqXQO6hKTZOzQ==");
            con.setDoOutput(true);
            con.setInstanceFollowRedirects(false);

            // Read the raw JSON to a string
            InputStreamReader in = new InputStreamReader((InputStream) con.getContent());
            BufferedReader buff = new BufferedReader(in);

            StringBuilder b = new StringBuilder();
            String line;
            do {
                line = buff.readLine();
                b.append(line + "\n");
            } while (line != null);

            return b.toString();
        } catch (MalformedURLException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

}
