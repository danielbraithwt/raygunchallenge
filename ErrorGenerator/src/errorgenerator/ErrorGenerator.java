package errorgenerator;

import com.mindscapehq.raygun4java.core.RaygunClient;

import java.awt.*;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.rmi.UnexpectedException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.util.Scanner;
import java.util.TooManyListenersException;

/**
 * Created by danielbraithwt on 9/25/15.
 */
public class ErrorGenerator {

    /**
     * Store the types of error messages you can throw
     * with there names
     */
    private enum ErrorType {
        HEADLESS_EXCEPTION("Headless Exception"),
        TO_MANY_LISTENERS_EXCEPTION("To Many Listeners Exception"),
        NO_SUCH_ALGORYTHM_EXCEPTION("No Such Algorythm Exception"),
        UNEXPECTED_EXCEPTION("Unexpected Exception"),
        UNRECOVERABLE_KEY_EXCEPTION("Unrecoverable Key Exception");

        private String name;

        ErrorType(String n) {
            name = n;
        }

        @Override
        public String toString() {
            return name;
        }
    }

    private static Scanner in;

    public static void main(String[] args) throws Throwable{
        // Setup raygun to catch and send errors
        Thread.setDefaultUncaughtExceptionHandler(new RaygunExceptionHandler());

        in = new Scanner(System.in);

        int option = getOption();

        // Based on selection send correct error to
        // raygun with message
        switch (ErrorType.values()[option]) {
            case HEADLESS_EXCEPTION:
                throw new HeadlessException("Java thinks you are to silly to be a programmer");

            case NO_SUCH_ALGORYTHM_EXCEPTION:
                throw new NoSuchAlgorithmException("Algorythm is to creative, please use an alterive algorythm");

            case UNEXPECTED_EXCEPTION:
                throw new UnexpectedException("Bet you wernt expecting this one");

            case UNRECOVERABLE_KEY_EXCEPTION:
                throw new UnrecoverableKeyException("You dropped your key into a drain");

            case TO_MANY_LISTENERS_EXCEPTION:
                throw new TooManyListenersException("There are to many connections on your router, the NSA is unable to listen in. Please upgrade your router");
        }

        in.close();
    }

    /**
     * Handles displaying the list of exceptions to the user
     * and getting which one they want to throw
     * @return
     */
    public static int getOption() {
        int option = -1;

        // While the option selected is invalid
        while (option == -1) {
            System.out.println("Please Select An Error:");

            // Show all the options
            int i = 0;
            for (ErrorType e : ErrorType.values()) {
                System.out.println(i + ") " + e.toString());
                i++;
            }

            System.out.println();
            System.out.print("What is your choice (0 - " + (ErrorType.values().length-1) + "): ");

            int input = in.nextInt();

            // Ensure the input is valid
            if (input >= 0 && input < ErrorType.values().length) {
                option = input;
            }
        }

        return option;
    }

    /**
     * Raygun error handler, catches errors and sends them to raygun
     */
    private static class RaygunExceptionHandler implements Thread.UncaughtExceptionHandler {
        @Override
        public void uncaughtException(Thread t, Throwable e) {
            System.out.println(e);

            RaygunClient c = new RaygunClient("0ALfOUzpenwr9fQKO0JeTg==");
            c.Send(e);
        }
    }
}
